﻿using System;

namespace Oop.Shapes.Factories
{
	public class ShapeFactory
	{
		public Shape CreateCircle(int radius)
		{
			return new Circle(radius);


			throw new NotImplementedException();
		}

		public Shape CreateTriangle(int a, int b, int c)
		{
			return new Triangle(a, b, c);
			throw new NotImplementedException();
		}

		public Shape CreateSquare(int a)
		{
			return new Square(a);
			throw new NotImplementedException();
		}

		public Shape CreateRectangle(int a, int b)
		{
			return new Rectangle(a, b);
			throw new NotImplementedException();
		}
	}
}