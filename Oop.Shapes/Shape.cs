﻿namespace Oop.Shapes
{
	abstract public class Shape
	{
		/// <summary>
		/// Площадь фигуры
		/// </summary>
		public virtual double Area { get; }

		/// <summary>
		/// Периметр
		/// </summary>
		abstract public double Perimeter { get; }

		/// <summary>
		/// Количество вершин
		/// </summary>
		abstract public  int VertexCount { get; }

		abstract public  bool IsEqual(Shape shape);
	}

	public class Circle : Shape
    {
		protected double radius;
		//protected double area;
		//protected double perimeter;
		//protected double vertexCount = 0;		 //по умолчанию у окружности нет вершин

		public Circle(double radius)
		{
			if (radius <= 0)
			{ throw new System.ArgumentOutOfRangeException(); }
			else { this.radius = radius; }
		}
		public override double Area
		{
			 get {return System.Math.PI * radius * radius; }
		}
        public override double Perimeter
		{
			get { return 2 * System.Math.PI * radius; }
		}
        public override int VertexCount
		{ get => 0; }

        public override bool IsEqual(Shape shape)
        {
			if (shape is Circle)
			{ if (shape.Area == this.Area)
					return true;
				else { return false; }
			} 
			else { return false; }
        }
    }

	public class Triangle:Shape
    {
		protected double a;
		protected double b;
		protected double c;

		public Triangle (double a, double b, double c)
        {
			if (a > 0 && b > 0 && c > 0
				&& (a + b > c) && (b + c > a) && (c + a > b)) 
			{
				this.a = a;
				this.b = b;
				this.c = c;
			}
			else if (a > 0 && b > 0 && c > 0 && ((a+b <= c) || (b +c <= a) || (c+a <= b))) 
			{ throw new System.InvalidOperationException(); }
			else { throw new System.ArgumentOutOfRangeException(); }
        }

		public override double Area
		{
			get {
					var p = 0.5 * (a + b + c);
					return System.Math.Sqrt(p * (p - a) * (p - b) * (p - c)); 
				}
		}
		public override double Perimeter
		{
			get { return a + b+ c; }
		}

		public override int VertexCount
		{ get => 3; }

		public override bool IsEqual(Shape shape)
		{
			if (shape is Triangle)
			{
				if (shape.Area == this.Area && shape.Perimeter == this.Perimeter)
					return true;
				else { return false; }
			}
			else { return false; }
		}

	}

	public class Square : Shape 
	{
		protected double a;

		public Square(double a)
		{
			if (a > 0)
			{ this.a = a; }
			else
			{
				throw new System.ArgumentOutOfRangeException();
			}
		}

		public override double Area
        {
			get { return a * a; }
        }

        public override double Perimeter
		{ get { return 4 * a; } }

		public override int VertexCount
		{ get => 4; }

		public override bool IsEqual(Shape shape)
		{
			if (shape is Square)
			{
				if (shape.Perimeter == this.Perimeter)
					return true;
				else { return false; }
			}
			else { return false; }
		}
	}

	public class Rectangle : Shape
    {
		protected double a;
		protected double b;

		public Rectangle(double a, double b)
        {
			if (a > 0 && b > 0)
            {
				this.a = a;
				this.b = b;
			}
			else { throw new System.ArgumentOutOfRangeException(); }
        }

		public override double Area
        {
            get { return a * b; }
        }

        public override double Perimeter
        {
			get { return 2 * a + 2 * b; }
        }

		public override int VertexCount
		{ get => 4; }

		public override bool IsEqual(Shape shape)
		{
			if (shape is Rectangle)
			{
				if (shape.Area == this.Area && shape.Perimeter == this.Perimeter)
					return true;
				else { return false; }
			}
			else { return false; }
		}
	}
}
